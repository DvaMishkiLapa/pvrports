# Maintainer: Jonathan Bakker <xc-racer2@live.ca>
pkgname=sgx-ddk-um
pkgver=1.17.4948957
_commit=94ea394473ea72db5e77cdf49e4bb3a8491ced09
pkgrel=1
pkgdesc="(WARNING: runs as root) Proprietary userspace libraries for PowerVR SGX GPUs"
url="https://git.ti.com/cgit/graphics/omap5-sgx-ddk-um-linux"
arch="armhf armv7 aarch64"
license="proprietary"
options="!check !strip" # no test suite available
makedepends="autoconf automake m4 libtool mesa-pvr-dri-classic-dev libdrm-dev"
depends="libc6-compat"
subpackages="
	$pkgname-ti343x:soc
	$pkgname-ti443x:soc
	$pkgname-openrc
	"
source="
	https://github.com/maemo-leste/sgx-ddk-um/archive/$_commit.tar.gz
	sgx-ddk-um.initd
	"
builddir="$srcdir/sgx-ddk-um-$_commit"

prepare() {
	default_prepare
	autoreconf -v -f -i
}

copy_soc_files() {
	local soc_name=$1
	local soc_path="$subpkgdir/usr/lib"
	local soc_bin="$subpkgdir/usr/bin"
	mkdir -p $soc_path
	mkdir -p $soc_bin

	cd $builddir
	cp -a $soc_name/usr/lib/* $soc_path/
	cp -a $soc_name/usr/bin/* $soc_bin/
}

build() {
	# Force detection of gbm library
	export GBM_LIBS=/usr/lib
	export GBM_CFLAGS=/usr/include
	./configure \
		--prefix=/usr \
		--sysconfdir=/etc \
		--localstatedir=/var \
		--sbindir=/usr/sbin 
	make
}

package() {
	make install DESTDIR="$pkgdir"
	install -Dm755 "$srcdir"/sgx-ddk-um.initd "$pkgdir"/etc/init.d/sgx-ddk-um
}

openrc() {
	install="$subpkgname.post-install $subpkgname.post-upgrade"
	default_openrc
}

soc() {
	local n=${subpkgname##sgx-ddk-um-}
	provides=sgx-ddk-um-soc=$pkgver
	depends=sgx-ddk-um-openrc

	copy_soc_files $n
}

sha512sums="
24dc362ed1d6214473633b13e75adc872c1c02afcb9bbf003e7f6035a99a545158b62ab2dc2809d02f451b1b433d0d0df9ba1848fcde655b895278f39d30fe5a  94ea394473ea72db5e77cdf49e4bb3a8491ced09.tar.gz
a917cee7fa9ada1deded9b70bdc34a921ac9bb0a8b3e401a6cbfba4d5f07fd725dee74a1e5ddc237a8f5753956ea7ae722405c0e1f694ad36dfea933fa1d23b7  sgx-ddk-um.initd
"
